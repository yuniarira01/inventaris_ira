<?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "ujikom";
 
    // membuat koneksi
    $koneksi = new mysqli($servername, $username, $password, $dbname);
 
    // melakukan pengecekan koneksi
    if ($koneksi->connect_error) {
        die("Connection failed: " . $koneksi->connect_error);
    } 
 
    if($_POST['rowid']) {
        $id_inventaris = $_POST['rowid'];
        // mengambil data berdasarkan id
    $sql= "select * from inventaris s   left join jenis p on p.id_jenis=s.id_jenis
										left join ruang r on r.id_ruang=s.id_ruang
										where id_inventaris=$id_inventaris	";
	$result=$koneksi->query($sql);
	foreach ($result as $baris){
		?>
            <table class="table table-striped responsive-utilities jambo_table bulk_action">
                <tr>
                    <td>Nama Barang</td>
                    <td>:</td>
                    <td><?php echo $baris['nama']; ?></td>
                </tr>
                    <td>Jumlah</td>
                    <td>:</td>
                    <td><?php echo $baris['jumlah']; ?></td>
                </tr>
				<tr>
                    <td>Nama Jenis</td>
                    <td>:</td>
                    <td><?php echo $baris['nama_jenis']; ?></td>
                </tr>
				<tr>
                    <td>Tanggal Register</td>
                    <td>:</td>
                    <td><?php echo $baris['tanggal_register']; ?></td>
                </tr>
				<tr>
                    <td>Nama Ruang</td>
                    <td>:</td>
                    <td><?php echo $baris['nama_ruang']; ?></td>
                </tr>
				<tr>
                    <td>Kode Inventaris</td>
                    <td>:</td>
                    <td><?php echo $baris['kode_inventaris']; ?></td>
                </tr>
			
            </table>
        <?php 
 
        }
    }
    $koneksi->close();
?>