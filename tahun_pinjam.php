<?php
include ('cek.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	

    <title>INVENTARIS| </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>
    <script src="js/nprogress.js"></script>
    <script>
        NProgress.start();
    </script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                 
                  
                    <!-- sidebar menu -->
                  	<?php 
					
					if ($_SESSION['id_level']==1){
						
						echo'<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                      
                        <div class="menu_section">
						 <div class="navbar nav_title" style="border: 0;">
                    <font color="white"><h1>Administrator</h1></font></span></a>
						<br/>
						<br/>
						<br/>
                    </div>
						<br>
						<br>
						
						
                        <ul class="nav side-menu">
				<br>
							  <li><a href="dashbor.php"><i class="fa fa-list-alt"></i>Beranda</a>
							  <li><a><i class="fa fa-info-circle"></i>Inventarisir<span class="fa fa-chevron-down"></span></a>
								  <ul class="nav child_menu" style="display: none">
								     <li><a href="barang.php">Barang</a>
                                        </li>
								   <li><a href="ruangan.php">Ruangan</a>
                                        </li>
										<li><a href="jenis.php">Jenis</a>
                                        </li>
                                    </ul>
								<li><a href="tampil_pinjam.php"><i class="fa fa-desktop"></i>Peminjaman</a>
								<li><a href="tampil_pengembalian.php"><i class="fa fa-desktop"></i>Pengembalian</a>
								<li><a href="laporan_peminjaman.php"><i class="fa fa-file-text-o"></i>Generate Laporan</a>
                                
								 <li><a><i class="fa fa-navicon"></i>Lainnya<span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="deskripsi.php">Deskripsi</a>
                                        </li>
										 <li><a href="view.php">View</a>
                                        </li>
										<li><a href="pegawai.php">Pegawai</a>
                                        </li>
											<li><a href="level.php">Level</a>
                                        </li>
										<li><a href="petugas.php">Petugas</a>
                                        </li>
									
                                    </ul>
                                </li>
								
                        </div>
                        
                                
                                
                            </ul>
                        </div><!---/penutup---->';
					}
					elseif ($_SESSION['id_level']==3){
						echo'<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
						<img width="30" height="120" src="images/15.jpg" alt="..." class="img-circle profile_img">
                            <ul class="nav side-menu">
								<li><a href="tampil_pinjam.php"><i class="fa fa-desktop"></i>Peminjaman</a>
                                </li
								<li><a href="logout.php"> <i class="fa fa-sign-out pull-right"></i>Logout</a>
                                    </li>		
                        </div>
						</ul>
                        </div><!---/penutup---->';
					}
					?>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    
                    <!-- /menu footer buttons -->
                </div>
            </div>
			

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle" >
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
					
						  <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                      			 <font color="white" size="2"><b><?php echo $_SESSION['nama_petugas']; ?></font></b>
                    
                                   <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                       

                            
                </div>

            </div>
            <!-- /top navigation -->


            <!-- page content -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row tile_count">
                    
         <p align="justify"><center><font  color="black"><h1><b>LAPORAN PEMINJAMAN</h1></p></b>
		 <div class="container">
            <div class="row">
                <div class="col-lg-12">
                   
						<center><div class="panel-body">
						<div class="col-lg-5">
						<label>Pilih Tahun</label>
							<form method="get">
							<select name="tahun" class="form-control m-bot15">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysql_query("SELECT YEAR(tanggal_pinjam) as tahun FROM peminjaman GROUP BY YEAR(tanggal_pinjam)");
								while($row = mysql_fetch_assoc($result))
								{
								echo "<option value='$row[tahun]'>$row[tahun]</option>";
								} 
								?>
									</select>
									<br/>
								<a href="laporan_peminjaman.php"><button type="button" class="btn btn-danger fa fa-reply-all"> Kembali</button></a>
								<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
							</form>
						</div>
					</div></center>
                        <!-- /.panel-heading -->
					
									 
						<?php if(isset($_GET['tahun'])){ ?>
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table id="tester" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>										
										<th>No</th>
										<th>Kode Pinjam</th>
										<th>Nama Barang</th>
										<th>Jumlah Pinjam</th>
										<th>Tanggal Pinjam</th>
										<th>Tanggal Kembali</th>
										<th>Nama Pegawai</th>									
                                        </tr>                            
            					</thead>
			 
									<tbody>
									<?php
									$no=1;
										$select=mysql_query("select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
             										 left join detail_pinjam  d on a.id_peminjaman=d.id_detail_pinjam 
													 left join inventaris i on i.id_inventaris=d.id_inventaris WHERE YEAR(tanggal_pinjam) = '$_GET[tahun]'");
								
									while($data=mysql_fetch_array($select))
									{
									?>
										<tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data['kode_pinjam']; ?></td>
										<td><?php echo $data['nama']; ?></td>
										<td><?php echo $data['jumlah_pinjam']; ?></td>										
										<td><?php echo $data['tanggal_pinjam']; ?></td>
										<td><?php echo $data['tanggal_kembali']; ?></td>										
										<td><?php echo $data['nama_pegawai']; ?></td>											
                                        </tr>
										
										<?php
									}
									?>
									
                                    </tbody>
                                </table>
								</div>
								<form method="POST" action="pdf_tahun.php">
						<input type="hidden" name="tahun" value="<?php echo $_GET['tahun']?>">
						<button type="submit" class="btn btn-primary fa fa-file-excel-o"> Pdf</button></form>
					
								<script type="text/javascript" src="assets/js/jquery.min.js"></script>
								<script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
								
								<script>
								$(document).ready(function() {
								$('#tester').DataTable();
								});
								</script>
		<tr>
			
            </div>
            <!-- /page content -->

        </div>

    </div>
						<?php } ?>
    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>

    <script src="js/bootstrap.min.js"></script>

    <!-- gauge js -->
    <script type="text/javascript" src="js/gauge/gauge.min.js"></script>
    <script type="text/javascript" src="js/gauge/gauge_demo.js"></script>
    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>
    <script>
        $(document).ready(function () {
            // [17, 74, 6, 39, 20, 85, 7]
            //[82, 23, 66, 9, 99, 6, 2]
            var data1 = [[gd(2012, 1, 1), 17], [gd(2012, 1, 2), 74], [gd(2012, 1, 3), 6], [gd(2012, 1, 4), 39], [gd(2012, 1, 5), 20], [gd(2012, 1, 6), 85], [gd(2012, 1, 7), 7]];

            var data2 = [[gd(2012, 1, 1), 82], [gd(2012, 1, 2), 23], [gd(2012, 1, 3), 66], [gd(2012, 1, 4), 9], [gd(2012, 1, 5), 119], [gd(2012, 1, 6), 6], [gd(2012, 1, 7), 9]];
            $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
                data1, data2
            ], {
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {
                    verticalLines: true,
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",
                    borderWidth: 1,
                    color: '#fff'
                },
                colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [1, "day"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                        //mode: "time", timeformat: "%m/%d/%y", minTickSize: [1, "day"]
                },
                yaxis: {
                    ticks: 8,
                    tickColor: "rgba(51, 51, 51, 0.06)",
                },
                tooltip: false
            });

            function gd(year, month, day) {
                return new Date(year, month - 1, day).getTime();
            }
        });
    </script>

    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
    <script>
        $(function () {
            $('#world-map-gdp').vectorMap({
                map: 'world_mill_en',
                backgroundColor: 'transparent',
                zoomOnScroll: false,
                series: {
                    regions: [{
                        values: gdpData,
                        scale: ['#E6F2F0', '#149B7E'],
                        normalizeFunction: 'polynomial'
                    }]
                },
                onRegionTipShow: function (e, el, code) {
                    el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
                }
            });
        });
    </script>
    <!-- skycons -->
    <script src="js/skycons/skycons.js"></script>
    <script>
        var icons = new Skycons({
                "color": "#73879C"
            }),
            list = [
                "clear-day", "clear-night", "partly-cloudy-day",
                "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
                "fog"
            ],
            i;

        for (i = list.length; i--;)
            icons.set(list[i], list[i]);

        icons.play();
    </script>

    <!-- dashbord linegraph -->
    <script>
        var doughnutData = [
            {
                value: 30,
                color: "#455C73"
            },
            {
                value: 30,
                color: "#9B59B6"
            },
            {
                value: 60,
                color: "#BDC3C7"
            },
            {
                value: 100,
                color: "#26B99A"
            },
            {
                value: 120,
                color: "#3498DB"
            }
    ];
        var myDoughnut = new Chart(document.getElementById("canvas1").getContext("2d")).Doughnut(doughnutData);
    </script>
    <!-- /dashbord linegraph -->
    <!-- datepicker -->
    <script type="text/javascript">
        $(document).ready(function () {

            var cb = function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
            }

            var optionSet1 = {
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                minDate: '01/01/2012',
                maxDate: '12/31/2015',
                dateLimit: {
                    days: 60
                },
                showDropdowns: true,
                showWeekNumbers: true,
                timePicker: false,
                timePickerIncrement: 1,
                timePicker12Hour: true,
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                },
                opens: 'left',
                buttonClasses: ['btn btn-default'],
                applyClass: 'btn-small btn-primary',
                cancelClass: 'btn-small',
                format: 'MM/DD/YYYY',
                separator: ' to ',
                locale: {
                    applyLabel: 'Submit',
                    cancelLabel: 'Clear',
                    fromLabel: 'From',
                    toLabel: 'To',
                    customRangeLabel: 'Custom',
                    daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                    monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                    firstDay: 1
                }
            };
            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
            $('#reportrange').daterangepicker(optionSet1, cb);
            $('#reportrange').on('show.daterangepicker', function () {
                console.log("show event fired");
            });
            $('#reportrange').on('hide.daterangepicker', function () {
                console.log("hide event fired");
            });
            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
            });
            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                console.log("cancel event fired");
            });
            $('#options1').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
            });
            $('#options2').click(function () {
                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
            });
            $('#destroy').click(function () {
                $('#reportrange').data('daterangepicker').remove();
            });
        });
    </script>
    <script>
        NProgress.done();
    </script>
    <!-- /datepicker -->
    <!-- /footer content -->
</body>

</html>

