<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Inventaris Sarana & Prasarana SMK</title>
        <!-- Bootstrap -->
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                     <a class="brand" href="beranda.php">Inventaris Sarana & Prasarana SMK</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                            <a href="inventarisir.php"><i class="icon-chevron-right"></i> Inventarisir</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						<li>
                            <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                        </li>
                        <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Lainya <i class="icon-chevron-right"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="jenis.php">Jenis</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="level.php">Level</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="pegawai.php">Pegawai</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="ruang.php">Ruang</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="beranda2.php">Beranda</a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==2){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}
			?>
                <!--/span-->
                <div class="span9" id="content">

                    
                    <div class="row-fluid">
                        <!-- block -->
                       
                    </div>
							<div class="page-header">
                                <font size="5" color="Gray"><b>Inventaris</b></font>
                            </div>
                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tambah Inventaris</div>
                            </div>
                        <div class="block-content collapse in">
                                <div class="span12">
								<?php
								include "koneksi.php";
								$query = "select max(kode_inventaris) as maxKode from inventaris";
								$hasil = mysql_query ($query);
								$data = mysql_fetch_array($hasil);
								$KodeBarang = $data['maxKode'];
								$noUrut = (int) substr($KodeBarang, 10, 10);
								$noUrut++;
								$char = "INVENTARIS";
								$KodeBarang = $char . sprintf("%01s",$noUrut);
								?>
                                    <form action="simpan_inventaris.php" method="post" class="form-horizontal">
                                      <fieldset>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Nama </label>
                                          <div class="controls">
                                            <input name="nama" type="text" class="span6" id="typeahead"  data-provide="typeahead"  data-items="4" required="" maxlength="50">
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Kondisi </label>
                                         <div class="controls">
                                            <input name="kondisi" type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" required="" maxlength="50">
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="textarea2">Keterangan</label>
                                          <div class="controls">
                                            <textarea name="keterangan_inventaris" class="input-xlarge textarea" placeholder="Masukan Keterangan" style="width: 700px; height: 200px" ></textarea>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                         <div class="controls">
                                            <input name="jumlah" type="number" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" required="" maxlength="5">
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="select01">Jenis</label>
                                          <div class="controls">
                                            <select name="id_jenis" id="select01" class="chzn-select">
											<option>pilih</option>
                                            <?php
											$con = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($con,"select id_jenis,nama_jenis from jenis ORDER BY id_jenis");
											while($row = mysqli_fetch_assoc($result))
											{
											echo "<option>$row[id_jenis].$row[nama_jenis]</option>";
											}
											?>
                                            </select>
                                          </div>
                                        </div>
                                       <div class="control-group">
                                          <label class="control-label" for="select01">Ruang</label>
                                          <div class="controls">
                                            <select name="id_ruang" id="select01" class="chzn-select">
                                           <option>pilih</option>
                                            <?php
											$con = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($con,"select id_ruang,nama_ruang from ruang ORDER BY id_ruang");
											while($row = mysqli_fetch_assoc($result))
											{
											echo "<option>$row[id_ruang].$row[nama_ruang]</option>";
											}
											?>
                                            </select>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Kode Inventaris </label>
                                          <div class="controls">
                                            <input name="kode_inventaris" type="text" class="span6" id="typeahead"  data-provide="typeahead" data-items="4" required="" value="<?php echo $KodeBarang;?>" readonly>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="select01">Petugas</label>
                                          <div class="controls">
                                            <select name="id_petugas" id="select01" class="chzn-select">
                                           <option>pilih</option>
                                            <?php
											$con = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($con,"select id_petugas,nama_petugas from petugas ORDER BY id_petugas");
											while($row = mysqli_fetch_assoc($result))
											{
											echo "<option>$row[id_petugas].$row[nama_petugas]</option>";
											}
											?>
                                            </select>
                                          </div>
                                        </div>
                                     <br>
									 <br>
									 <br>
									 <br>
									 <br>
									 <br>
                                          <button type="submit" class="btn btn-primary">Save</button>
                                          <button type="reset" class="btn">Cancel</button>
                         
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>
			</div>
            <hr>
            <footer>
                <p>Inventaris Sarana & Prasarana SMK</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">

        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen">

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

	<script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/form-validation.js"></script>
        
	<script src="assets/scripts.js"></script>
        <script>

	jQuery(document).ready(function() {   
	   FormValidation.init();
	});
	

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
    </body>

</html>