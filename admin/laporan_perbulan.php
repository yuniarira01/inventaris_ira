<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Inventaris Sarana & Prasarana SMK</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="beranda.php">Inventaris Sarana & Prasarana SMK</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                            <a href="inventarisir.php"><i class="icon-chevron-right"></i> Inventarisir</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						<li>
                            <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                        </li>
                        <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Lainya <i class="icon-chevron-right"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="jenis.php">Jenis</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="level.php">Level</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="pegawai.php">Pegawai</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="ruang.php">Ruang</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="beranda2.php">Beranda</a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==2){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}
			?>
                <!--/span-->
                <div class="span9" id="content">

                    
                    <div class="row-fluid">
                        <!-- block -->
                       
                    </div>
							<div class="page-header">
                                <font size="5" color="Gray"><b>Inventaris</b></font>
                            </div>
                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Laporan</div>
                                
                            </div>
                            <div class="block-content collapse in">
                                <div class="row-fluid padd-bottom">
                                  
								<div class="bootstrap-admin-panel-content">
								<p align="center"> <font size="5" color="Gray"><b>Laporan </b></font></P>
								<div class="row-fluid">
                        <div class="span4">
								<a href="laporan_pertanggal.php">
								<div class="span12" align="center">
								<IMG width="100" height="100"  src="images/file3.png">
								<p hspace="12"/><font color="black"><b>Laporan Pertanggal</b></font></img>	
								</div>
								</a>
                        </div>
						<div class="span4">
								<a href="laporan_perbulan.php">
								<div class="span12" align="center">
								<IMG width="100" height="100"  src="images/file3.png">
								<p hspace="12"/><font color="black"><b>Laporan Perbulan</b></font></img>	
								</div>
								</a>
                        </div> <div class="span4">
								<a href="laporan_pertahun.php">
								<div class="span12" align="center">
								<IMG width="100" height="100"  src="images/file3.png">
								<p hspace="12"/><font color="black"><b>Laporan Pertahun</b></font></img>	
								</div>
								</a>
                        </div>
                    </div>	
					<br>
					<br>
								<center>
							
                               <div class="panel-body">
						<div class="col-lg-5">
						<label>Pilih Perbulan</label>
							<form method="get">
							<select name="bulan" class="form-control m-bot15">
								<?php
								include "koneksi.php";
								//display values in combobox/dropdown
								$result = mysql_query("SELECT month(tanggal_pinjam) as bulan FROM peminjaman GROUP BY MONTH(tanggal_pinjam)");
								while($row = mysql_fetch_assoc($result))
								{
								echo "<option value='$row[bulan]'>$row[bulan]</option>";
								} 
								?>
							</select>
									<br/>
								<button type="submit" class="btn btn-outline btn-primary">Tampilkan</button>
							</form>
						</div>
					</div>
		</center>

<br>
									
				 <?php if(isset($_GET['bulan'])) { ?>
		<br>
                                   
                                        <form method="POST" action="print_laporanperbulan.php">
										<input type="hidden" name="bulan" value="<?php echo $_GET['bulan']?>">
										<button type="submit" class="btn btn-info"><i class="icon-file icon-white"></i> Export To PDF</button></a>
                                     </div>
                                  <table id="example" class="table table-striped table-bordered table-hover">
                                    <thead>
                                         <tr>
										<th>No.</th>
										<th>ID Peminjaman</th>
										<th>Nama Barang</th>
										<th>Jumlah Pinjam</th>
										<th>Tanggal Pinjam</th>
										<th>Tanggal Kembali</th>
										<th>Nama Pegawai</th>
							
                                        </tr> 
                                 <tbody>
									<?php
									$no=1;
										$select=mysql_query("select * from peminjaman a left join pegawai b on b.id_pegawai=a.id_pegawai
																left join detail_pinjam d on a.id_peminjaman=d.id_detail_pinjam
																left join inventaris i on i.id_inventaris=d.id_inventaris WHERE MONTH(tanggal_pinjam) = '$_GET[bulan]'");
									
									while($data=mysql_fetch_array($select))
									{
									?>
                                      <tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data['id_peminjaman']; ?></td>
										<td><?php echo $data['nama']; ?></td>
										<td><?php echo $data['jumlah_pinjam']; ?></td>
										<td><?php echo $data['tanggal_pinjam']; ?></td>
										<td><?php echo $data['tanggal_kembali']; ?></td>
										<td><?php echo $data['nama_pegawai']; ?></td>
                                        </tr>
										<?php
									}
									?>
                                    </tbody>
									  </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
					<?php
					}
					?>
                </div>
            </div>
			</div>
			</div>
			</div>
			</div>
			</div>
            <hr>
            <footer>
                <p>Inventaris Sarana & Prasarana SMK</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>