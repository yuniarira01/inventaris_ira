<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Inventaris Sarana & Prasarana SMK</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <link href="assets/DT_bootstrap.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="beranda.php">Inventaris Sarana & Prasarana SMK</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                            <a href="inventarisir.php"><i class="icon-chevron-right"></i> Inventarisir</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						<li>
                            <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                        </li>
                        <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Lainya <i class="icon-chevron-right"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="jenis.php">Jenis</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="level.php">Level</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="pegawai.php">Pegawai</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="ruang.php">Ruang</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="beranda2.php">Beranda</a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==2){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}
			?>
                <!--/span-->
                <div class="span9" id="content">

                    
                    <div class="row-fluid">
                        <!-- block -->
                       
                    </div>
							<div class="page-header">
                                <font size="5" color="Gray"><b>Inventaris</b></font>
                            </div>
                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                           <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tabel Peminjaman</div>
                        
                            </div>
                            <div class="block-content collapse in">
                            <div class="row-fluid padd-bottom">
							<br>
							
							<div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                        <center><div class="panel-body">
                        <div class="col-lg-3 col-md-offset-4">
                <label>Pilih Id Peminjaman</label>
                <form method="POST">
                    <select name="id_peminjaman" class="form-control m-bot15">
                        <?php
                        include "koneksi.php";
                                //display values in combobox/dropdown
                        $result = mysql_query("SELECT id_peminjaman from peminjaman where status_peminjaman='Pinjam' ");
                        while($row = mysql_fetch_assoc($result))
                        {
                            echo "<option value='$row[id_peminjaman]'>$row[id_peminjaman]</option>";
                        } 
                        ?>
                   </select>
                                    <br/>
                                <button type="submit" name="pilih" class="btn btn-outline btn-primary">Tampilkan</button>
                            </form>
                        </div>
                    </div></center>
					
	
                <?php
                if(isset($_POST['pilih'])){?>
                  <form action="proses_pengembalian.php" method="post" enctype="multipart/form-data">
                     <?php
                     include "koneksi.php";
                     $id_peminjaman=$_POST['id_peminjaman'];
                     $select=mysql_query("select * from peminjaman left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
					 left join inventaris on detail_pinjam.id_inventaris=inventaris.id_inventaris where id_peminjaman='$id_peminjaman'");
                     while($data=mysql_fetch_array($select)){
                        ?>
                <div class="form-group">
                    <label>Id Inventaris</label>
                    <input name="id_peminjaman" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_peminjaman'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_detail_pinjam" type="hidden" class="form-control" placeholder="Masukan Jumlah barang" value="<?php echo $data['id_detail_pinjam'];?>" autocomplete="off" maxlength="11" required="">
                    <input name="id_inventaris" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>.<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" required="" readonly>
                </div><br>                    
               
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Tanggal Pinjam
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="datetime" name="" value="<?php echo $data['tanggal_pinjam']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="tanggal_pinjam">
                        <input type="hidden" name="tanggal_pinjam" value="<?php echo $data['tanggal_pinjam']?>" >
                        <input type="hidden" name="tanggal_kembali" >
                    </div>
                </div><br><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        ID Pegawai
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="" value="<?php echo $data['id_pegawai']?>" class="form-control col-md-7 col-xs-12" disabled placeholder="ID Pegawai">
                        <input type="hidden" name="id_pegawai" value="<?php echo $data['id_pegawai']?>" >
                    </div>
                </div><br><br><br>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                        Jumlah
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="number" name="jumlah" value="<?php echo $data['jumlah_pinjam']?>" class="form-control col-md-7 col-xs-12"  placeholder="Jumlah" readonly>
                    </div>
                </div><br><br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" >
                     Status Peminjaman
                 </label>
                 <div class="col-md-6 col-sm-6 col-xs-12">
                  <select name="status_peminjaman" class="form-control m-bot15">
                    <option><?php echo $data['status_peminjaman']?></option>
                    <option>Kembali</option>

                </select>
            </div><br><br><br>
            <button type="submit" class="btn">Simpan</button>
        </div>

    <?php } ?>
</form>

<?php } ?>

</div>
</div>
</div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                   <div class="table-toolbar">
                                      <div class="btn-group">
                                      </div>
                                      <div class="btn-group pull-right">
                                         <button data-toggle="dropdown" class="btn dropdown-toggle">Export <span class="caret"></span></button>
                                         <ul class="dropdown-menu">
                                            <li><a href="print_pengembalian.php">Export To PDF</a></li>
                                            <li><a href="proses_excelpengembalian.php">Export To Excel</a></li>
                                         </ul>
                                      </div>
                                   </div>
                                    
                                    <table id="example" class="table table-striped table-bordered table-hover">
                                    <thead>
                                      <tr>
										<th><p align="center">No.</p></th>
										<th><p align="center">ID Peminjaman</p></th>
										<th><p align="center">Tanggal Kembali</p></th>
										<th><p align="center">Status Peminjaman</p></th>
										<th><p align="center">Pegawai</p></th>
							
                                        </tr> 
                                 <tbody>
									<?php
                                    include "koneksi.php";
                                    $no=1;
                                    $select=mysql_query("select * from peminjaman left join detail_pinjam on peminjaman.id_peminjaman=detail_pinjam.id_detail_pinjam
									left join inventaris on inventaris.id_inventaris=detail_pinjam.id_inventaris
									left join pegawai on peminjaman.id_pegawai=pegawai.id_pegawai where status_peminjaman='Kembali'");
                                    while($data=mysql_fetch_array($select))
                                    {
                                    ?>
                                      <tr>
										<td><?php echo $no++; ?></td>
										<td><?php echo $data['id_peminjaman']; ?></td>
										<td><?php echo $data['tanggal_kembali']; ?></td>
										<td><?php echo $data['status_peminjaman']; ?></td>
										<td><?php echo $data['nama_pegawai']; ?></td>
										
										  
									  </tr>
										<?php
									}
									?>
                                    </tbody>
									  </table>
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>
		</div>
		</div>
            <hr>
            <footer>
                <p>Inventaris Sarana & Prasarana SMK</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
    </body>

</html>