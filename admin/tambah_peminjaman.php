<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html>
    
    <head>
        <title>Inventaris Sarana & Prasarana SMK</title>
        <!-- Bootstrap -->
      <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                     <a class="brand" href="beranda.php">Inventaris Sarana & Prasarana SMK</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                      
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                 <?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                            <a href="inventarisir.php"><i class="icon-chevron-right"></i> Inventarisir</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						<li>
                            <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                        </li>
                        <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Lainya <i class="icon-chevron-right"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="jenis.php">Jenis</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="level.php">Level</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="pegawai.php">Pegawai</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="ruang.php">Ruang</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="beranda2.php">Beranda</a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==2){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}
			?>
                <!--/span-->
                <div class="span9" id="content">

                    
                    <div class="row-fluid">
                        <!-- block -->
                       
                    </div>
							<div class="page-header">
                                <font size="5" color="Gray"><b>Inventaris</b></font>
                            </div>
                     <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Peminjaman</div>
                            </div>
                        <div class="block-content collapse in">
                                <div class="span12">
								<?php
								$select_inventaris=mysql_query("SELECT `id_inventaris`,`nama` FROM `inventaris`");
								$select_pegawai=mysql_query("SELECT `id_inventaris,`nama` FROM `pegawai`");
								?>
                                    <form action="simpan_peminjaman.php" method="post" class="form-horizontal">
                                      <fieldset>
										<div class="control-group">
                                          <label class="control-label" for="typeahead">Jumlah </label>
                                         <div class="controls">
                                            <input name="jumlah" type="number" id="typeahead"  data-provide="typeahead" data-items="4" required="" maxlength="5">
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="select01">Inventaris</label>
                                          <div class="controls">
                                            <select name="id_inventaris" id="select01" class="chzn-select">
                                             <option>pilih</option>
                                            <?php
											$con = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($con,"select id_inventaris,nama from inventaris ORDER BY id_inventaris");
											while($row = mysqli_fetch_assoc($result))
											{
											echo "<option>$row[id_inventaris].$row[nama]</option>";
											}
											?>
                                            </select>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="select01">Status Peminjaman</label>
                                          <div class="controls">
                                            <select name="status_peminjaman" id="select01" class="chzn-select">
											  <option>Pinjam</option>
                                            </select>
                                          </div>
                                        </div>
										<div class="control-group">
                                          <label class="control-label" for="select01">Pegawai</label>
                                          <div class="controls">
                                            <select name="id_pegawai" id="select01" class="chzn-select" >
											<option>pilih</option>
                                            <?php
											$con = mysqli_connect("localhost","root","","ujikom");
											$result = mysqli_query($con,"select id_pegawai,nama_pegawai from pegawai ORDER BY id_pegawai");
											while($row = mysqli_fetch_assoc($result))
											{
											echo "<option>$row[id_pegawai].$row[nama_pegawai]</option>";
											}
											?>
                                            </select>
                                          </div>
                                        </div>
                                     
                                          <button type="submit" class="btn btn-primary">Save</button>
                                          <button type="reset" class="btn">Cancel</button>
                         
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        <!-- /block -->
                    </div>
                </div>
            </div>
		</div>
            <hr>
            <footer>
                <p>Inventaris Sarana & Prasarana SMK</p>
            </footer>
        </div>
        <!--/.fluid-container-->

        <script src="vendors/jquery-1.9.1.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/datatables/js/jquery.dataTables.min.js"></script>


        <script src="assets/scripts.js"></script>
        <script src="assets/DT_bootstrap.js"></script>
        <script>
        $(function() {
            
        });
        </script>
		 <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
      
    </body>

</html>