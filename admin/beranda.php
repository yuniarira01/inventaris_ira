<?php
include ('cek.php');
error_reporting(0);
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>Inventaris Sarana & Prasarana SMK</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                     <a class="brand" href="beranda.php">Inventaris Sarana & Prasarana SMK</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo $_SESSION['nama_petugas'];?><i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
			
                <?php
			if ($_SESSION['id_level']==1){
				
				
			echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                            <a href="inventarisir.php"><i class="icon-chevron-right"></i> Inventarisir</a>
                        </li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
						<li>
                            <a href="laporan.php"><i class="icon-chevron-right"></i> Laporan</a>
                        </li>
                        <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">Lainya <i class="icon-chevron-right"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="jenis.php">Jenis</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="level.php">Level</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="pegawai.php">Pegawai</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="petugas.php">Petugas</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="ruang.php">Ruang</a>
                                    </li>
									<li>
                                        <a tabindex="-1" href="beranda2.php">Beranda</a>
                                    </li>
                                </ul>
                            </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==2){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                       <li>
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        <li>
                            <a href="pengembalian.php"><i class="icon-chevron-right"></i> Pengembalian</a>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}else if ($_SESSION['id_level']==3){
				
				echo'<div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="peminjaman.php"><i class="icon-chevron-right"></i> Peminjaman</a>
                        </li>
                        </li>
                    </ul>
			</div><!---penutup---->';
			
			}
			?>
                <!--/span-->
                <div class="span9" id="content">
                       
                    <div class="row-fluid">
					<div class="col-lg-12">
                            <div class="page-header">
                                <font size="5" color="Gray"><b>Inventaris</b></font>
                            </div>
                        </div>
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"></div>
                                <div class="pull-right">

                                </div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="row-fluid padd-bottom">
                                  
								<div class="bootstrap-admin-panel-content">
								<p align="center"> <font size="5" color="Gray"><b>Selamat Datang</b></font></P>
                                <div class="main-content">
								  
		<?php
        include "koneksi.php";
        $no=1;
        $select=mysql_query("select * from beranda");
        while($data=mysql_fetch_array($select))
        {
        ?>
            <tr>
				
	            <td><p align="center"><font size="3"><?php echo $data['deskripsi']; ?></font></p></td>
				<br>
				<br>
			</tr>
		<?php
		}
		?>
								</div>  
                                </div>
								  
                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>
                </div>
				</div>
            <hr>
            <footer>
                <p>Inventaris Sarana & Prasarana SMK</p>
            </footer>
        </div>
		</div>
        <!--/.fluid-container-->
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
    </body>

</html>